# 6.0001 Problem Set 3
#
# The 6.0001 Word Game
# Created by: Kevin Luu <luuk> and Jenna Wiens <jwiens>
#
# Name          : <your name>
# Collaborators : <your collaborators>
# Time spent    : <total time>

import copy
import math
import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1, 'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10,'*':0
}

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)

WORDLIST_FILENAME = "words.txt"

def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.append(line.strip().lower())
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def get_frequency_dict(sequence):
    """
    Returns a dictionary where the keys are elements of the sequence
    and the values are integer counts, for the number of times that
    an element is repeated in the sequence.

    sequence: string or list
    return: dictionary
    """
    
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x,0) + 1
    return freq
	

# (end of helper code)
# -----------------------------------

#
# Problem #1: Scoring a word
#



def get_word_score(word: str, n: int) -> int:
    points = 0
    for letter in word: points += SCRABBLE_LETTER_VALUES[letter] 
    points *= abs((7 * len(word)) - (3 * (n - len(word))))
        
    return points
    


#
# Make sure you understand how this function works and what it does!
#
def display_hand(hand: dict):
    """
    Displays the letters currently in the hand.

    For example:
       display_hand({'a':1, 'x':2, 'l':3, 'e':1})
    Should print out something like:
       a x x l l l e
    The order of the letters is unimportant.

    hand: dictionary (string -> int)
    """
    
    for letter in hand.keys():
        for j in range(hand[letter]):
             print(letter, end=' ')      # print all on the same line
    print()                              # print an empty line

#
# Make sure you understand how this function works and what it does!
# You will need to modify this for Problem #4.

def deal_hand(n: int) -> dict:
    """
    Returns a random hand containing n lowercase letters.
    ceil(n/3) letters in the hand should be VOWELS (note,
    ceil(n/3) means the smallest integer not less than n/3).

    Hands are represented as dictionaries. The keys are
    letters and the values are the number of times the
    particular letter is repeated in that hand.

    n: int >= 0
    returns: dictionary (string -> int)
    """
    
    hand={}
    num_vowels = int(math.ceil(n / 3))

    for i in range(num_vowels):
        x = random.choice(VOWELS)
        hand[x] = hand.get(x, 0) + 1
    
    for i in range(num_vowels, n):    
        x = random.choice(CONSONANTS)
        hand[x] = hand.get(x, 0) + 1
    hand['*'] = 1
    l = list(hand.items())
    random.shuffle(l)
    hand = dict(l)
    
    return hand

#
# Problem #2: Update a hand by removing letters
#

def update_hand(hand: dict, word: str):
    for letter in word:
        if letter in hand:
            del_letters[letter] = del_letters.get(letter, 0) + 1
            hand[letter] -= 1
            if hand[letter] == 0:
                del(hand[letter])
    """
    word: string
    hand: dictionary (string -> int)    
    returns: dictionary (string -> int)
    """
    
def string_hand(hand:dict) -> str:
    '''
        This code converts type dict to type string.
        Return: new_hand type string
    '''
    str_hand = []
    for i in hand.keys():
        for j in range(hand[i]):
             str_hand.append(i)
    str_hand = ''.join(hand)
    return str_hand 



#
# Problem #3: Test word validity
#


def is_valid_word(word: str, hand: dict , word_list: list) -> bool:
    count = 0 
    sec_hand = string_hand(hand)
    if (word in word_list) or exam_word:
        for letter in word:
            if letter in sec_hand or letter == '*':
                sec_hand = sec_hand.replace(letter,'',1)
                count += 1
    if count == len(word): return True        
    return False
    
    """
    Returns True if word is in the word_list and is entirely
    composed of letters in the hand. Otherwise, returns False.
    Does not mutate hand or word_list.
   
    word: string
    hand: dictionary (string -> int)
    word_list: list of lowercase strings
    returns: boolean
    """

#
# Problem #5: Playing a hand
#

def wildcards(word_list: list,word: str):
    for i in VOWELS:
        change_word = word
        change_word = change_word.replace('*',i)
        if change_word in word_list: return True
    return False 


def play_hand(hand: dict, word_list: str, n: int) -> int:
    global exam_word
    
    total_points = 0
    print('Current hand: ', end = '')
    display_hand(hand)
    
    while True:
        word = str(input("Please enter a word or '!!' to indicate you are done: ").lower())

        if word == '!!':
            print(f'Total score for this hand: {total_points}')
            return total_points
        
        if '*' in word:
            exam_word = wildcards(word_list,word)
   
        else: exam_word = False
        
        if is_valid_word(word,hand,word_list):
            total_points += get_word_score(word,n)
            print(f'"{word}" earned {get_word_score(word,n)}. Total score {total_points} ')
            update_hand(hand,word)
            print('\n''Current hand: ', end = '')
            display_hand(hand)
            n -= len(word)
            
            if n == 0:
                print('Ran out of letters')
                print(f'Total score for this hand: {total_points}')
                return total_points
        else:
            print('This word is not in hand''\n')
            update_hand(hand,word)
            n -= len(word)
            
            if n > 0:
                print('Current hand: ', end = '')
                display_hand(hand)
                
            elif n == 0:
                print('Ran out of letters')
                print(f'Total score for this hand: {total_points}')
                return total_points
        
    """
    Allows the user to play the given hand, as follows:

    * The hand is displayed.
    
    * The user may input a word.

    * When any word is entered (valid or invalid), it uses up letters
      from the hand.

    * An invalid word is rejected, and a message is displayed asking
      the user to choose another word.

    * After every valid word: the score for that word is displayed,
      the remaining letters in the hand are displayed, and the user
      is asked to input another word.

    * The sum of the word scores is displayed when the hand finishes.

    * The hand finishes when there are no more unused letters.
      The user can also finish playing the hand by inputing two 
      exclamation points (the string '!!') instead of a word.

      hand: dictionary (string -> int)
      word_list: list of lowercase strings
      returns: the total score for the hand
      
    """


    
    # BEGIN PSEUDOCODE <-- Remove this comment when you implement this function
    # Keep track of the total score
    
    # As long as there are still letters left in the hand:
    
        # Display the hand
        
        # Ask user for input
        
        # If the input is two exclamation points:
        
            # End the game (break out of the loop)

            
        # Otherwise (the input is not two exclamation points):

            # If the word is valid:

                # Tell the user how many points the word earned,
                # and the updated total score

            # Otherwise (the word is not valid):
                # Reject invalid word (print a message)
                
            # update the user's hand by removing the letters of their inputted word
            

    # Game is over (user entered '!!' or ran out of letters),
    # so tell user the total score

    # Return the total score as result of function



#
# Problem #6: Playing a game
# 


#
# procedure you will use to substitute a letter in a hand
#

def substitute_hand(hand: dict, letter: str) -> dict:
    hand2 = {}
    if letter not in hand: print('This letter not in hand')
    for i in hand:
        if i == letter: hand2[random.choice(CONSONANTS + VOWELS)] = hand[letter]
        else: hand2[i] = hand[i]
        
    return hand2     
   

    """ 
    Allow the user to replace all copies of one letter in the hand (chosen by user)
    with a new letter chosen from the VOWELS and CONSONANTS at random. The new letter
    should be different from user's choice, and should not be any of the letters
    already in the hand.

    If user provide a letter not in the hand, the hand should be the same.

    Has no side effects: does not mutate hand.

    For example:
        substitute_hand({'h':1, 'e':1, 'l':2, 'o':1}, 'l')
    might return:
        {'h':1, 'e':1, 'o':1, 'x':2} -> if the new letter is 'x'
    The new letter should not be 'h', 'e', 'l', or 'o' since those letters were
    already in the hand.
    
    hand: dictionary (string -> int)
    letter: string
    returns: dictionary (string -> int)
    """
          
    
def play_game(word_list):
    global n , del_letters
    del_letters = {}
    n = 6
    hand = deal_hand(n)
    quest = ''
    c = 0
    while quest != 'no':
        if  c != 0:
            hand.update(del_letters)
            del_letters = {}
            n = 7
            print('\n''Current hand: ', end = '')
            display_hand(hand)
            req = str(input('Would you like to substitute a letter (yes / no)?').lower())

            if req == 'yes':
                letter = str(input('Which letter would you like to replace: ').lower())
                hand = substitute_hand(hand,letter)
            play_hand(hand,word_list,n)
            quest = input('Would you like to replay the hand (yes / no)').lower()
            if quest == 'no': break

        else:
            n = 7
            print('\n''Current hand: ', end = '')
            display_hand(hand)
            req = str(input('Would you like to substitute a letter (yes / no)?').lower())

            if req == 'yes':
                letter = str(input('Which letter would you like to replace: ').lower())
                hand = substitute_hand(hand,letter)

            play_hand(hand,word_list,n)
            quest = input('Would you like to replay the hand (yes / no)').lower()
            c += 1


        
        
        
        




    """
    Allow the user to play a series of hands

    * Asks the user to input a total number of hands

    * Accumulates the score for each hand into a total score for the 
      entire series
 
    * For each hand, before playing, ask the user if they want to substitute
      one letter for another. If the user inputs 'yes', prompt them for their
      desired letter. This can only be done once during the game. Once the
      substitue option is used, the user should not be asked if they want to
      substitute letters in the future.

    * For each hand, ask the user if they would like to replay the hand.
      If the user inputs 'yes', they will replay the hand and keep 
      the better of the two scores for that hand.  This can only be done once 
      during the game. Once the replay option is used, the user should not
      be asked if they want to replay future hands. Replaying the hand does
      not count as one of the total number of hands the user initially
      wanted to play.

            * Note: if you replay a hand, you do not get the option to substitute
                    a letter - you must play whatever hand you just had.
      
    * Returns the total score for the series of hands

    word_list: list of lowercase strings
    """

    


#
# Build data structures used for entire session and play game
# Do not remove the "if __name__ == '__main__':" line - this code is executed
# when the program is run directly, instead of through an import statement
#
if __name__ == '__main__':
    word_list = load_words()
    play_game(word_list)
